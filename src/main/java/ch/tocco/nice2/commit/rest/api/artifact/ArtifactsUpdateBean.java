package ch.tocco.nice2.commit.rest.api.artifact;

import java.util.List;
import java.util.Map;
import java.util.Objects;

public class ArtifactsUpdateBean {
    private Map<String, List<String>> artifacts;

    public Map<String, List<String>> getArtifacts() {
        return artifacts;
    }

    public void setArtifacts(Map<String, List<String>> artifacts) {
        this.artifacts = artifacts;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ArtifactsUpdateBean that = (ArtifactsUpdateBean) o;
        return Objects.equals(artifacts, that.artifacts);
    }

    @Override
    public int hashCode() {
        return Objects.hash(artifacts);
    }
}
