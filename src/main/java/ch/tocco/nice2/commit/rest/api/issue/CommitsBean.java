package ch.tocco.nice2.commit.rest.api.issue;

import java.util.List;

import ch.tocco.nice2.commit.git.Commit;

public class CommitsBean {

    private List<? extends Commit> commits;

    public CommitsBean() {
    }

    public CommitsBean(List<? extends Commit> commits) {
        this.commits = commits;
    }

    public void setCommits(List<? extends Commit> commits) {
        this.commits = commits;
    }

    public List<? extends Commit> getCommits() {
        return commits;
    }
}
