package ch.tocco.nice2.commit.rest.api.artifact;

import ch.tocco.nice2.commit.tocco.ArtifactConnector;
import ch.tocco.nice2.commit.tocco.ArtifactList;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/artifact")
public class ArtifactController {
    private final ArtifactConnector artifactConnector;

    public ArtifactController(ArtifactConnector artifactConnector) {
        this.artifactConnector = artifactConnector;
    }

    @GetMapping("/{modelName}")
    public ArtifactList getAllArtifacts(@PathVariable String modelName) {
        return artifactConnector.getAllArtifacts(modelName);
    }

    @GetMapping("/issue/{issueKey}")
    public ArtifactsBean getArtifacts(@PathVariable String issueKey) {
        ArtifactsBean artifactsBean = new ArtifactsBean();
        artifactsBean.setArtifacts(artifactConnector.getArtifactsForIssue(issueKey));
        return artifactsBean;
    }

    @PutMapping("/issue/{issueKey}")
    public void putArtifacts(@PathVariable String issueKey,
                             @RequestBody ArtifactsUpdateBean artifacts) {
        artifactConnector.updateArtifactLinks(issueKey, artifacts);
    }
}
