package ch.tocco.nice2.commit.rest.api.artifact;

import ch.tocco.nice2.commit.tocco.Artifact;

import java.util.List;
import java.util.Map;

public class ArtifactsBean {
    private Map<String, List<Artifact>> artifacts;

    public Map<String, List<Artifact>> getArtifacts() {
        return artifacts;
    }

    public void setArtifacts(Map<String, List<Artifact>> artifacts) {
        this.artifacts = artifacts;
    }
}
