package ch.tocco.nice2.commit.rest.api.installation;

import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ch.tocco.nice2.commit.git.BackendGitConnector;
import ch.tocco.nice2.commit.git.ClientGitConnector;
import ch.tocco.nice2.commit.git.Commit;
import ch.tocco.nice2.commit.git.CommitInstalled;
import ch.tocco.nice2.commit.jira.Issue;
import ch.tocco.nice2.commit.jira.JiraConnector;
import ch.tocco.nice2.commit.rest.api.NotFoundException;
import ch.tocco.nice2.commit.rest.api.issue.CommitsBean;
import ch.tocco.nice2.commit.tocco.Installation;
import ch.tocco.nice2.commit.tocco.InstallationList;
import ch.tocco.nice2.commit.tocco.ToccoConnector;
import ch.tocco.nice2.commit.tocco.VersionBean;

@RestController
@RequestMapping(path = "/installation")
public class InstallationController {
    private final ToccoConnector toccoConnector;
    private final BackendGitConnector backendGitConnector;
    private final ClientGitConnector clientGitConnector;
    private final JiraConnector jiraConnector;

    public InstallationController(ToccoConnector toccoConnector,
                                  BackendGitConnector backendGitConnector,
                                  ClientGitConnector clientGitConnector,
                                  JiraConnector jiraConnector) {
        this.toccoConnector = toccoConnector;
        this.backendGitConnector = backendGitConnector;
        this.clientGitConnector = clientGitConnector;
        this.jiraConnector = jiraConnector;
    }

    @GetMapping
    public InstallationList listInstallation() {
        return toccoConnector.loadAllInstallations();
    }

    @GetMapping(path = "/{instance}")
    public Installation installation(@PathVariable String instance) {
        return toccoConnector.loadInstallation(instance)
            .orElseThrow(() -> new NotFoundException("Installation not found: " + instance));
    }

    @GetMapping(path = "/{instance}/client/issue/{key}")
    public CommitsBean getClientCommits(@PathVariable String instance, @PathVariable String key) {
        Issue issue = jiraConnector.fetchIssue(key)
                .orElseThrow(() -> new NotFoundException("Issue not found: " + key));

        Installation installation = toccoConnector.loadInstallation(instance)
                .orElseThrow(() -> new NotFoundException("Installation not found: " + instance));

        VersionBean version = toccoConnector.getVersion(installation);

        if (version.getAdminVersion() == null) {
            return new CommitsBean(List.of());
        }

        List<CommitInstalled> clientCommits = clientGitConnector.searchCommitsForInstallation(version, issue.getKey() + "\\b");
        return new CommitsBean(clientCommits);
    }

    @GetMapping(path = "/{instance}/commit/{hash}")
    public CommitInstalledBean isCommitInstalled(@PathVariable String instance, @PathVariable String hash) {
        Installation installation = toccoConnector.loadInstallation(instance)
            .orElseThrow(() -> new NotFoundException("Installation not found: " + instance));

        Commit commit = backendGitConnector.retrieveCommit(hash)
            .orElseThrow(() -> new NotFoundException("No commit found with hash: " + hash));
        String installedRevision = toccoConnector.getInstalledRevision(installation);
        boolean installed = backendGitConnector.isAncestor(commit.getCommitId(), installedRevision);

        CommitInstalledBean bean = new CommitInstalledBean();

        bean.setCommitHash(hash);
        bean.setCommitMessage(commit.getCommitMessage());
        bean.setInstallation(installation.getLabel());
        bean.setInstalled(installed);

        return bean;
    }
}
