package ch.tocco.nice2.commit.rest;

public class RestConnectionException extends RuntimeException {

    public RestConnectionException(String message, Throwable cause) {
        super(message, cause);
    }
}
