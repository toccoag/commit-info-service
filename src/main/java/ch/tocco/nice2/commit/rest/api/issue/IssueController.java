package ch.tocco.nice2.commit.rest.api.issue;

import java.util.List;

import com.google.common.base.Strings;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import ch.tocco.nice2.commit.git.BackendGitConnector;
import ch.tocco.nice2.commit.git.ClientGitConnector;
import ch.tocco.nice2.commit.git.Commit;
import ch.tocco.nice2.commit.jira.Issue;
import ch.tocco.nice2.commit.jira.IssueList;
import ch.tocco.nice2.commit.jira.JiraConnector;
import ch.tocco.nice2.commit.rest.api.BadRequestException;
import ch.tocco.nice2.commit.rest.api.NotFoundException;

@RestController
@RequestMapping(path = "/issue")
public class IssueController {

    private final JiraConnector jiraConnector;
    private final BackendGitConnector backendGitConnector;
    private final ClientGitConnector clientGitConnector;

    public IssueController(JiraConnector jiraConnector,
                           BackendGitConnector backendGitConnector,
                           ClientGitConnector clientGitConnector) {
        this.jiraConnector = jiraConnector;
        this.backendGitConnector = backendGitConnector;
        this.clientGitConnector = clientGitConnector;
    }

    @RequestMapping(path = "/search", method = RequestMethod.POST)
    public IssueList searchIssues(@RequestBody SearchBean searchBean) {
        if (Strings.isNullOrEmpty(searchBean.getSearchTerm())) {
            throw new BadRequestException("Search term is required");
        }

        return jiraConnector.searchIssues(searchBean.getSearchTerm());
    }

    @RequestMapping(path = "/{key}", method = RequestMethod.GET)
    public Issue issue(@PathVariable String key) {
        return jiraConnector.fetchIssue(key)
            .orElseThrow(() -> new NotFoundException("Issue not found: " + key));
    }

    @RequestMapping(path = "/{key}/commit", method = RequestMethod.GET)
    public CommitsBean commitsOfIssue(@PathVariable String key) {
        Issue issue = jiraConnector.fetchIssue(key)
            .orElseThrow(() -> new NotFoundException("Issue not found: " + key));
        List<Commit> backendCommits = backendGitConnector.searchCommits(issue.getKey() + "\\b");
        List<Commit> clientCommits = clientGitConnector.searchCommits(issue.getKey() + "\\b");
        return new CommitsBean(Lists.newArrayList(Iterables.concat(backendCommits, clientCommits)));
    }
}
