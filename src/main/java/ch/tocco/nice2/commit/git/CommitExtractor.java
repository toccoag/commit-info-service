package ch.tocco.nice2.commit.git;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.function.Function;

public class CommitExtractor implements Function<InputStream, List<Commit>> {

    private final GitCommitParser commitParser;
    private final Repo repo;

    public CommitExtractor(GitCommitParser commitParser, Repo repo) {
        this.commitParser = commitParser;
        this.repo = repo;
    }

    @Override
    public List<Commit> apply(InputStream inputStream) {
        try {
            return commitParser.parseCommits(repo, inputStream);
        } catch (IOException e) {
            throw new GitException(e);
        }
    }
}
