package ch.tocco.nice2.commit.git;

public class GitException extends RuntimeException {
    public GitException(String message) {
        super(message);
    }
    public GitException(Throwable cause) {
        super(cause);
    }
    public GitException(String message, Throwable cause) {
        super(message, cause);
    }
}
