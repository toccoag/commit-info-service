package ch.tocco.nice2.commit.git;

import java.util.Objects;

public class CommitInstalled extends Commit {
    private boolean installed;

    public CommitInstalled() {
    }

    public CommitInstalled(Commit commit) {
        super(commit.getCommitId(),
                commit.getAuthor(),
                commit.getCommitTimestamp(),
                commit.getCommitMessage(),
                commit.getJiraIssueKeys(),
                commit.getRepo());
    }

    public boolean isInstalled() {
        return installed;
    }

    public void setInstalled(boolean installed) {
        this.installed = installed;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        CommitInstalled that = (CommitInstalled) o;
        return installed == that.installed;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), installed);
    }
}
