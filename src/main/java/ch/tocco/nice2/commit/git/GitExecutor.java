package ch.tocco.nice2.commit.git;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.slf4j.Logger;

import com.google.common.io.CharStreams;

public abstract class GitExecutor {
    private static final List<String> CLONE = List.of(
            "git", "clone", "--bare", "%repositorySource%", "%repositoryPath%"
    );
    private final ExecutorService executorService = Executors.newFixedThreadPool(2);

    private final String repositoryPath;
    private final String repositorySource;
    private final Logger log;

    public GitExecutor(String repositoryPath, String repositorySource, Logger log) {
        this.repositoryPath = repositoryPath;
        this.repositorySource = repositorySource;
        this.log = log;
    }

    public void executeAndProcessError(List<String> command) throws IOException, InterruptedException {
        executeAndProcessError(command, new ThrowawayFunction(), Collections.emptyMap());
    }

    public void executeAndProcessError(List<String> command, Map<String, String> arguments) throws IOException, InterruptedException {
        executeAndProcessError(command, new ThrowawayFunction(), arguments);
    }

    public <T> T executeAndProcessError(List<String> command, Function<InputStream, T> inputFunction, Map<String, String> arguments) throws IOException, InterruptedException {
        Process process = executeGitCommand(command, arguments);

        Future<T> successFuture = executorService.submit(() -> inputFunction.apply(process.getInputStream()));
        Future<String> failureFuture = executorService.submit(() -> new StringCollector().apply(process.getErrorStream()));

        try {
            int exitCode = process.waitFor();
            if (exitCode == 0) {
                return successFuture.get();
            } else {
                throw new GitException(String.format("Error while executing the git command: ExitCode: %d%n%s", exitCode, failureFuture.get()));
            }
        } catch (ExecutionException e) {
            throw new GitException("Error executing git command", e);
        } finally {
            process.destroy(); //closes all streams
        }
    }

    public Process executeGitCommand(List<String> command, Map<String, String> arguments) throws IOException {
        command = replaceCommandArguments(command, arguments);
        log.info(String.format("executing git command: %s", String.join(" ", command)));
        return new ProcessBuilder(command)
                .directory(new File(repositoryPath))
                .start();
    }

    public void cloneRepositoryIfEmpty() throws IOException, InterruptedException {
        if (isDirectoryEmpty(repositoryPath)) {
            log.info("initialising git repository");
            executeAndProcessError(CLONE, Map.of(
                    "%repositoryPath%", repositoryPath,
                    "%repositorySource%", repositorySource
            ));
            log.info("finished git repository cloning");
        } else {
            log.info("git repository already cloned");
        }
    }

    private List<String> replaceCommandArguments(List<String> command, Map<String, String> arguments) {
        return command.stream()
                .map(s -> replaceArguments(s, arguments))
                .collect(Collectors.toList());
    }

    private String replaceArguments(String input, Map<String, String> arguments) {
        for (String argument : arguments.keySet()) {
            input = input.replace(argument, arguments.get(argument));
        }
        return input;
    }

    private boolean isDirectoryEmpty(String path) throws IOException {
        Path directory = Paths.get(path);
        try (DirectoryStream directoryStream = Files.newDirectoryStream(directory)) {
            return !directoryStream.iterator().hasNext();
        }
    }

    private static class StringCollector implements Function<InputStream, String> {

        @Override
        public String apply(InputStream inputStream) {
            try (InputStreamReader inputStreamReader = new InputStreamReader(inputStream)) {
                return CharStreams.toString(inputStreamReader);
            } catch (IOException e) {
                throw new GitException(e);
            }
        }
    }

    private static class ThrowawayFunction implements Function<InputStream, Void> {

        @Override
        public Void apply(InputStream inputStream) {
            try {
                //noinspection unused
                int data;
                //noinspection StatementWithEmptyBody,UnusedAssignment
                while ((data = inputStream.read()) != -1) {
                    //do nothing
                }
            } catch (IOException e) {
                throw new GitException(e);
            }

            return null;
        }
    }
}
