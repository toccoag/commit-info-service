package ch.tocco.nice2.commit.git;

import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class ClientGitExecutor extends GitExecutor {
    public ClientGitExecutor(@Value("${git.client.repository.path}") String backendRepositoryPath,
                             @Value("${git.client.repository.source}") String backendRepositorySource) {
        super(backendRepositoryPath, backendRepositorySource, LoggerFactory.getLogger(ClientGitExecutor.class));
    }
}
