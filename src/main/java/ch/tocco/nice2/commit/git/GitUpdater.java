package ch.tocco.nice2.commit.git;

import java.io.IOException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class GitUpdater {
    private static final List<String> FETCH = List.of("git", "fetch", "origin", "+refs/heads/*:refs/heads/*", "--prune");

    private final Logger log = LoggerFactory.getLogger(GitUpdater.class);

    private final BackendGitExecutor backendGitExecutor;
    private final ClientGitExecutor clientGitExecutor;

    private boolean readyForFetch = false;

    @Autowired
    public GitUpdater(BackendGitExecutor backendGitExecutor, ClientGitExecutor clientGitExecutor) {
        this.backendGitExecutor = backendGitExecutor;
        this.clientGitExecutor = clientGitExecutor;
    }


    @EventListener(ApplicationReadyEvent.class)
    public void setupRepository() {
        try {
            log.info("application ready. initialising git repository");
            backendGitExecutor.cloneRepositoryIfEmpty();
            clientGitExecutor.cloneRepositoryIfEmpty();
        } catch (InterruptedException | IOException e) {
            throw new GitException(e);
        } finally {
            readyForFetch = true;
        }
    }

    @Scheduled(fixedDelay = 60000)
    public void fetch() {
        if (readyForFetch) {
            try {
                backendGitExecutor.executeAndProcessError(FETCH);
                clientGitExecutor.executeAndProcessError(FETCH);
            } catch (IOException | InterruptedException e) {
                throw new GitException(e);
            }
        }
    }
}
