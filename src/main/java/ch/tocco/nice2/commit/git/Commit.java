package ch.tocco.nice2.commit.git;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;

public class Commit {
    private String commitId;
    private String author;
    private LocalDateTime commitTimestamp;
    private String commitMessage;
    private List<String> jiraIssueKeys;
    private Repo repo;

    public Commit() {
    }

    public Commit(String commitId, String author, LocalDateTime commitTimestamp, String commitMessage, List<String> jiraIssueKeys, Repo repo) {
        this.commitId = commitId;
        this.author = author;
        this.commitTimestamp = commitTimestamp;
        this.commitMessage = commitMessage;
        this.jiraIssueKeys = jiraIssueKeys;
        this.repo = repo;
    }

    public String getCommitId() {
        return commitId;
    }

    public void setCommitId(String commitId) {
        this.commitId = commitId;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public LocalDateTime getCommitTimestamp() {
        return commitTimestamp;
    }

    public void setCommitTimestamp(LocalDateTime commitTimestamp) {
        this.commitTimestamp = commitTimestamp;
    }

    public String getCommitMessage() {
        return commitMessage;
    }

    public void setCommitMessage(String commitMessage) {
        this.commitMessage = commitMessage;
    }

    public List<String> getJiraIssueKeys() {
        return jiraIssueKeys;
    }

    public void setJiraIssueKeys(List<String> jiraIssueKeys) {
        this.jiraIssueKeys = jiraIssueKeys;
    }

    public Repo getRepo() {
        return repo;
    }

    public void setRepo(Repo repo) {
        this.repo = repo;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Commit commit = (Commit) o;
        return Objects.equals(commitId, commit.commitId) &&
            Objects.equals(author, commit.author) &&
            Objects.equals(commitTimestamp, commit.commitTimestamp) &&
            Objects.equals(commitMessage, commit.commitMessage) &&
            Objects.equals(jiraIssueKeys, commit.jiraIssueKeys) &&
            Objects.equals(repo, commit.repo);
    }

    @Override
    public int hashCode() {
        return Objects.hash(commitId, author, commitTimestamp, commitMessage, jiraIssueKeys, repo);
    }
}
