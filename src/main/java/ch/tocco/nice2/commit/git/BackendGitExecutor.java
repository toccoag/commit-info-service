package ch.tocco.nice2.commit.git;

import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class BackendGitExecutor extends GitExecutor {
    public BackendGitExecutor(@Value("${git.backend.repository.path}") String backendRepositoryPath,
                              @Value("${git.backend.repository.source}") String backendRepositorySource) {
        super(backendRepositoryPath, backendRepositorySource, LoggerFactory.getLogger(BackendGitExecutor.class));
    }
}
