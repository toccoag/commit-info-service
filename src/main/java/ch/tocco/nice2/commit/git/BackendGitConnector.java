package ch.tocco.nice2.commit.git;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class BackendGitConnector extends AbstractGitConnector {
    private static final List<String> GIT_LOG_COMMIT_ID = List.of("git", "log", "--abbrev-commit", "--full-index", "--date=iso-strict",  "-n", "1", "%commitId%");
    private static final List<String> GIT_LOG_SEARCH = List.of("git", "log", "--all", "--abbrev-commit", "--full-index", "--no-merges", "--date=iso-strict", "--grep=%regexp%");

    private final GitCommitParser commitParser;
    private final Repo repo = Repo.BACKEND;

    @Autowired
    public BackendGitConnector(BackendGitExecutor backendGitExecutor,
                               GitCommitParser commitParser) {
        super(backendGitExecutor);
        this.commitParser = commitParser;
    }

    public Optional<Commit> retrieveCommit(String commitId) {
        try {
            List<Commit> commits = gitExecutor.executeAndProcessError(GIT_LOG_COMMIT_ID, new CommitExtractor(commitParser, repo), Map.of("%commitId%", commitId));
            if (commits.size() == 1) {
                return Optional.of(commits.get(0));
            } else {
                throw new GitException("Unable to retrieve exactly one git commit");
            }
        } catch (InterruptedException | IOException e) {
            throw new GitException(e);
        } catch (GitException e) {
            if (e.getMessage().contains("unknown revision")) {
                return Optional.empty();
            }

            throw e;
        }
    }

    public List<Commit> searchCommits(String regexp) {
        try {
            return gitExecutor.executeAndProcessError(GIT_LOG_SEARCH, new CommitExtractor(commitParser, repo), Map.of("%regexp%", regexp));
        } catch (InterruptedException | IOException e) {
            throw new GitException(e);
        }
    }
}
