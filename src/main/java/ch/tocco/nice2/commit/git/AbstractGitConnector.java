package ch.tocco.nice2.commit.git;

import java.io.IOException;
import java.util.List;
import java.util.Map;

public abstract class AbstractGitConnector {
    private static final List<String> GIT_MERGE_BASE = List.of("git", "merge-base", "--is-ancestor", "%rev1%", "%rev2%");

    protected final GitExecutor gitExecutor;

    public AbstractGitConnector(GitExecutor gitExecutor) {
        this.gitExecutor = gitExecutor;
    }

    public boolean isAncestor(String rev1, String rev2) {
        try {
            Process process = gitExecutor.executeGitCommand(GIT_MERGE_BASE, Map.of("%rev1%", rev1, "%rev2%", rev2));
            return process.waitFor() == 0;
        } catch (InterruptedException | IOException e) {
            throw new GitException(e);
        }
    }
}
