package ch.tocco.nice2.commit.git;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import com.google.common.base.Charsets;
import com.google.common.io.ByteSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import ch.tocco.nice2.commit.tocco.VersionBean;

@Component
public class ClientGitConnector extends AbstractGitConnector {
    private static final List<String> GIT_LOG_PACKAGE_COMMIT_ID = List.of("git", "log", "%branch%", "--grep=%package-version%", "-n", "1", "--pretty=format:%h");
    private static final List<String> GIT_LOG_SEARCH = List.of("git", "log", "%branch%", "--abbrev-commit", "--full-index", "--no-merges", "--date=iso-strict", "--grep=%regexp%");
    private static final List<String> GIT_BRANCH_EXISTS = List.of("git", "rev-parse", "--verify", "%branch%");

    private final GitCommitParser commitParser;
    private final Repo repo = Repo.CLIENT;

    @Autowired
    public ClientGitConnector(ClientGitExecutor clientGitExecutor, GitCommitParser commitParser) {
        super(clientGitExecutor);
        this.commitParser = commitParser;
    }

    public List<CommitInstalled> searchCommitsForInstallation(VersionBean version, String regexp) {
        String branchName = getBranchName(version);
        List<Commit> commits = searchCommits(regexp, branchName);
        String commitIdPackageVersion = getCommitIdOfPackageVersion(branchName, String.format("tocco-admin@%s", version.getAdminVersion()));

        return commits.stream().map(c -> {
            CommitInstalled commitInstalled = new CommitInstalled(c);
            commitInstalled.setInstalled(isAncestor(c.getCommitId(), commitIdPackageVersion));
            return commitInstalled;
        }).collect(Collectors.toList());
    }

    public List<Commit> searchCommits(String regexp) {
        return searchCommits(regexp, "master");
    }

    private List<Commit> searchCommits(String regexp, String branch) {
        try {
            return gitExecutor.executeAndProcessError(GIT_LOG_SEARCH, new CommitExtractor(commitParser, repo), Map.of("%branch%", branch, "%regexp%", regexp));
        } catch (InterruptedException | IOException e) {
            throw new GitException(e);
        }
    }

    private String getBranchName(VersionBean version) {
        String branchName = String.format("nice-releases/%s", version.getVersion().replace(".", ""));

        try {
            Process process = gitExecutor.executeGitCommand(GIT_BRANCH_EXISTS, Map.of("%branch%", branchName));
            int exitCode = process.waitFor();
            if (exitCode != 0) {
                branchName = "master";
            }
        } catch (InterruptedException | IOException e) {
            throw new GitException(e);
        }

        return branchName;
    }

    private String getCommitIdOfPackageVersion(String branch, String packageVersion) {
        try {
            return gitExecutor.executeAndProcessError(GIT_LOG_PACKAGE_COMMIT_ID, new ResultExtractor(), Map.of("%branch%", branch, "%package-version%", packageVersion)).trim();
        } catch (InterruptedException | IOException e) {
            throw new GitException(e);
        }
    }

    private static class ResultExtractor implements Function<InputStream, String> {

        @Override
        public String apply(InputStream inputStream) {
            try {
                ByteSource byteSource = new ByteSource() {
                    @Override
                    public InputStream openStream() {
                        return inputStream;
                    }
                };

                return byteSource.asCharSource(Charsets.UTF_8).read();
            } catch (IOException e) {
                throw new GitException(e);
            }
        }
    }
}
