package ch.tocco.nice2.commit.git;

import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.stereotype.Service;

@Service
public class GitCommitParser {
    public List<Commit> parseCommits(Repo repo, InputStream inputStream) throws IOException {
        List<Commit> commits = Lists.newArrayList();

        String line;
        StringBuffer messageBuffer = null;
        Commit currentCommit = null;

        try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream))) {
            while ((line = bufferedReader.readLine()) != null) {
                if (line.startsWith("commit ")) {
                    if (currentCommit != null) {
                        commits.add(finalizeCommit(repo, currentCommit, messageBuffer));
                    }
                    currentCommit = new Commit();
                    messageBuffer = new StringBuffer();

                    currentCommit.setCommitId(parseCommitId(line));
                } else if (line.startsWith("Author: ")) {
                    if (currentCommit != null) {
                        currentCommit.setAuthor(parseAuthor(line));
                    }
                } else if (line.startsWith("Date: ")) {
                    if (currentCommit != null) {
                        currentCommit.setCommitTimestamp(parseCommitTimestamp(line));
                    }
                } else if (line.startsWith("    ")) {
                    if (messageBuffer != null) {
                        appendToMessage(messageBuffer, line);
                    }
                }
            }
            if (currentCommit != null) {
                commits.add(finalizeCommit(repo, currentCommit, messageBuffer));
            }
        }
        return commits;
    }

    private String parseCommitId(String commitIdLine) {
        String[] parts = commitIdLine.split(" ");
        return parts[1];
    }

    private String parseAuthor(String authorLine) {
        return authorLine.substring("Author: ".length()).trim();
    }

    private LocalDateTime parseCommitTimestamp(String dateLine) {
        String dateString = dateLine.substring("Date: ".length()).trim();
        return LocalDateTime.parse(dateString, DateTimeFormatter.ISO_DATE_TIME);
    }

    private void appendToMessage(StringBuffer messageBuffer, String line) {
        if (!Strings.isNullOrEmpty(messageBuffer.toString())) {
            messageBuffer.append(System.getProperty("line.separator"));
        }
        messageBuffer.append(line.substring(4));
    }

    private List<String> parseJiraIssues(String commitMessage) {
        return Arrays.stream(commitMessage.split(System.lineSeparator()))
            .map(String::toUpperCase)
            .filter(line -> line.startsWith("REFS:") || line.startsWith("CLOSES:"))
            .map(line -> line.replace("REFS:", ""))
            .map(line -> line.replace("CLOSES:", ""))
            .map(String::trim)
            .flatMap(line -> Arrays.stream(line.split(",")))
            .map(String::trim)
            .filter(issueKey -> issueKey.matches("[A-Z0-9]+-[0-9]+"))
            .collect(Collectors.toList());
    }

    private Commit finalizeCommit(Repo repo, Commit currentCommit, StringBuffer messageBuffer) {
        currentCommit.setCommitMessage(messageBuffer.toString());
        currentCommit.setJiraIssueKeys(parseJiraIssues(currentCommit.getCommitMessage()));
        currentCommit.setRepo(repo);
        return currentCommit;
    }
}
