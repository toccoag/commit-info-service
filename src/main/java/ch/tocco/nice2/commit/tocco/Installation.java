package ch.tocco.nice2.commit.tocco;

import java.util.Map;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Installation {

    private String instance;
    private String label;
    private String url;

    public Installation() {

    }

    public Installation(String instance,
                        String label,
                        String url) {
        this.instance = instance;
        this.label = label;
        this.url = url;
    }

    @JsonProperty("paths")
    public void handlePaths(Map<String, Object> fields) {
        this.instance = readPath("instance", fields);
        this.label = readPath("label", fields);
        this.url = readPath("url", fields);
    }

    @SuppressWarnings("unchecked")
    private String readPath(String path, Map<String, Object> fields) {
        Map<String, Object> pathMap = (Map<String, Object>) fields.get(path);
        return pathMap.get("value").toString();
    }

    public String getInstance() {
        return instance;
    }

    public String getLabel() {
        return label;
    }

    public String getUrl() {
        return url;
    }

    @Override
    public String toString() {
        return label;
    }
}
