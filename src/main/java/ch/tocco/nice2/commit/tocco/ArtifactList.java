package ch.tocco.nice2.commit.tocco;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ArtifactList {

    private List<Artifact> artifacts = List.of();

    public List<Artifact> getArtifacts() {
        return artifacts;
    }

    @JsonProperty("data")
    public void setArtifacts(List<Artifact> artifacts) {
        this.artifacts = artifacts;
    }
}
