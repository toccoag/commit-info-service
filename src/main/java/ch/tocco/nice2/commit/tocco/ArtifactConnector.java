package ch.tocco.nice2.commit.tocco;

import ch.tocco.nice2.commit.rest.api.artifact.ArtifactsUpdateBean;

import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.util.*;
import java.util.stream.Collectors;

@Component
public class ArtifactConnector {
    private final RestTemplate toccoRestTemplate;

    @Value("${tocco.api.url}")
    private String url;

    @Value("${tocco.api.pageSize}")
    private int pageSize;

    public ArtifactConnector(RestTemplate toccoRestTemplate) {
        this.toccoRestTemplate = toccoRestTemplate;
    }

    public Map<String, List<Artifact>> getArtifactsForIssue(String issueKey) {
        return Arrays.stream(ArtifactType.values())
                .collect(Collectors.toMap(ArtifactType::getModelName,
                        type -> getArtifactsForTypeAndIssue(type, issueKey).getArtifacts()));
    }

    private ArtifactList getArtifactsForTypeAndIssue(ArtifactType type, String issueKey) {
        return loadArtifactsPaged(
            type.getModelName(),
            Optional.of(String.format("relJira_issue.issue_key == string:\"%s\"", issueKey))
        );
    }

    public void updateArtifactLinks(String issueKey, ArtifactsUpdateBean artifacts) {
        String uri = UriComponentsBuilder.fromUriString(String.format("%s/tocco/artifact-update/%s", url, issueKey)).toUriString();
        toccoRestTemplate.put(URI.create(uri), artifacts);
    }

    public ArtifactList getAllArtifacts(String modelName) {
        return loadArtifactsPaged(modelName, Optional.empty());
    }

    private ArtifactList loadArtifactsPaged(String modelName, Optional<String> where) {
        UriComponentsBuilder countBuilder = UriComponentsBuilder.fromUriString(String.format("%s/entities/2.0/%s/count", url, modelName));
        where.ifPresent(w -> countBuilder.queryParam("_where", w));

        Count count = toccoRestTemplate.getForObject(countBuilder.toUriString(), Count.class);
        List<Artifact> result = Lists.newArrayList();

        if (count != null) {
            while (result.size() < count.count()) {
                UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromUriString(String.format("%s/entities/2.0/%s", url, modelName))
                    .queryParam("_paths", "pk,nr,label,relModule.label,relModule.relModule_type.label")
                    .queryParam("_sort", "nr")
                    .queryParam("_limit", pageSize)
                    .queryParam("_offset", result.size());

                where.ifPresent(w -> uriBuilder.queryParam("_where", w));

                ArtifactList artifacts = toccoRestTemplate.getForObject(uriBuilder.toUriString(), ArtifactList.class);
                if (artifacts != null) {
                    result.addAll(artifacts.getArtifacts());
                } else {
                    break;
                }
            }
        }

        ArtifactList artifactList = new ArtifactList();
        artifactList.setArtifacts(result);
        return artifactList;
    }
}
