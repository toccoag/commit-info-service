package ch.tocco.nice2.commit.tocco;

public enum ArtifactType {
    GENERAL("Artefact"),
    ACTION("Artefact_action"),
    BATCHJOB("Artefact_batchjob"),
    DIRECTIVE("Artefact_directive"),
    FLOW("Artefact_flow"),
    LISTENER("Artefact_listener"),
    REPORT("Artefact_report"),
    VALIDATOR("Artefact_validator");

    private final String modelName;

    ArtifactType(String modelName) {
        this.modelName = modelName;
    }

    public String getModelName() {
        return modelName;
    }
}
