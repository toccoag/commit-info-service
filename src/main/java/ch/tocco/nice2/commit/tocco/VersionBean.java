package ch.tocco.nice2.commit.tocco;

public class VersionBean {
    private String version;
    private String adminVersion;

    public VersionBean() {
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getAdminVersion() {
        return adminVersion;
    }

    public void setAdminVersion(String adminVersion) {
        this.adminVersion = adminVersion;
    }
}
