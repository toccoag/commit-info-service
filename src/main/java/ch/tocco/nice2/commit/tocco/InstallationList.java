package ch.tocco.nice2.commit.tocco;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class InstallationList {

    private List<Installation> installations = List.of();

    public List<Installation> getInstallations() {
        return installations;
    }

    @JsonProperty("data")
    public void setInstallations(List<Installation> issues) {
        this.installations = issues;
    }
}
