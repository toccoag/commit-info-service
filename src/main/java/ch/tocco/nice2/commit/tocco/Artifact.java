package ch.tocco.nice2.commit.tocco;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Artifact {
    private String key;
    private String nr;
    private String label;
    private String moduleLabel;
    private String moduleTypeLabel;

    public Artifact() {

    }

    public Artifact(String key, String nr, String label, String moduleLabel, String moduleTypeLabel) {
        this.key = key;
        this.nr = nr;
        this.label = label;
        this.moduleLabel = moduleLabel;
        this.moduleTypeLabel = moduleTypeLabel;
    }

    @JsonProperty("paths")
    public void handlePaths(Map<String, Object> fields) {
        this.nr = readPath("nr", fields);
        this.label = readPath("label", fields);
        this.moduleLabel = readPathRecursive("relModule.label", fields);
        this.moduleTypeLabel = readPathRecursive("relModule.relModule_type.label", fields);
    }

    @SuppressWarnings("unchecked")
    private String readPathRecursive(String path, Map<String, Object> fields) {
        if (fields.get("paths") != null) {
            fields = (Map<String, Object>) fields.get("paths");
        }

        List<String> parts = Arrays.asList(path.split("\\."));
        if (parts.size() == 1) {
            return readPath(path, fields);
        } else {
            fields = (Map<String, Object>) fields.get(parts.get(0));
            fields = (Map<String, Object>) fields.get("value");

            return readPathRecursive(String.join(".", parts.subList(1, parts.size())), fields);
        }
    }

    @SuppressWarnings("unchecked")
    private String readPath(String path, Map<String, Object> fields) {
        Map<String, Object> pathMap = (Map<String, Object>) fields.get(path);
        return pathMap.get("value").toString();
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getNr() {
        return nr;
    }

    public void setNr(String nr) {
        this.nr = nr;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getModuleLabel() {
        return moduleLabel;
    }

    public void setModuleLabel(String moduleLabel) {
        this.moduleLabel = moduleLabel;
    }

    public String getModuleTypeLabel() {
        return moduleTypeLabel;
    }

    public void setModuleTypeLabel(String moduleTypeLabel) {
        this.moduleTypeLabel = moduleTypeLabel;
    }
}
