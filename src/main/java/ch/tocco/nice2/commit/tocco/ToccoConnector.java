package ch.tocco.nice2.commit.tocco;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Optional;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Supplier;
import com.google.common.base.Suppliers;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

@Component
@SuppressWarnings("Guava")
public class ToccoConnector {
    private final Logger log = LoggerFactory.getLogger(ToccoConnector.class);

    private final RestTemplate toccoRestTemplate;
    private final RestTemplate statusToccoRestTemplate;

    @Value("${tocco.api.url}")
    private String url;

    private Supplier<InstallationList> cache;

    public ToccoConnector(RestTemplate toccoRestTemplate,
                          RestTemplate statusToccoRestTemplate) {
        this.toccoRestTemplate = toccoRestTemplate;
        this.statusToccoRestTemplate = statusToccoRestTemplate;
        cache = createSupplier();
    }

    private Supplier<InstallationList> createSupplier() {
        return Suppliers.memoizeWithExpiration(this::doLoadAllInstallations, 6, TimeUnit.HOURS);
    }

    public InstallationList loadAllInstallations() {
        return cache.get();
    }

    private InstallationList doLoadAllInstallations() {
        String uri = UriComponentsBuilder.fromUriString(url + "/entities/2.0/Installation")
            .queryParam("_paths", "instance,label,url")
            .queryParam("_where", "relInstallation_status.unique_id == string:\"active\" or relInstallation_status.unique_id == string:\"pilot\"")
            .queryParam("_sort", "instance")
            .queryParam("_limit", 1000)
            .toUriString();

        return toccoRestTemplate.getForObject(uri, InstallationList.class);
    }

    public Optional<Installation> loadInstallation(String instance) {
        return cache.get()
            .getInstallations()
            .stream()
            .filter(i -> instance.equals(i.getInstance()))
            .findAny();
    }

    public void resetCache() {
        cache = createSupplier();
    }

    public VersionBean getVersion(Installation installation) {
        String status = statusToccoRestTemplate.getForObject(buildInstallationUrl(installation, "/status-tocco"), String.class);

        VersionBean bean = new VersionBean();

        Matcher matcherVersion = Pattern.compile(">Version: ([0-9.]*)<").matcher(status);
        if (matcherVersion.find()) {
            bean.setVersion(matcherVersion.group(1));
        } else {
            throw new IllegalStateException("Version not found for " + installation.getLabel());
        }

        Matcher matcherAdminVersion = Pattern.compile(">tocco-admin: ([0-9a-zA-z.-]*)<").matcher(status);
        if (matcherAdminVersion.find()) {
            bean.setAdminVersion(matcherAdminVersion.group(1));
        }

        return bean;
    }

    public String getInstalledRevision(Installation installation) {
        String status = statusToccoRestTemplate.getForObject(buildInstallationUrl(installation, "/status-tocco"), String.class);

        Matcher matcher = Pattern.compile(">Revision: ([0-9a-f]*)<").matcher(status);
        if (matcher.find()) {
            return matcher.group(1);
        }

        throw new IllegalStateException("Revision not found for " + installation.getLabel());
    }

    //the url in the backoffice sometimes points to the root, sometimes to /tocco...
    private String buildInstallationUrl(Installation installation, String path) {
        try {
            URI uri = new URI(installation.getUrl());
            return new URI(uri.getScheme(), uri.getHost(), path, null).toString();
        } catch (URISyntaxException e) {
            throw new IllegalStateException("Illegal installation uri: " + installation.getUrl());
        }
    }
}
