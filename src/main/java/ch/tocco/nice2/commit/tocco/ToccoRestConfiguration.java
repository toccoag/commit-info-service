package ch.tocco.nice2.commit.tocco;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

import ch.tocco.nice2.commit.rest.AbstractRestConfiguration;

@Configuration
public class ToccoRestConfiguration extends AbstractRestConfiguration {

    @Value("${tocco.api.username}")
    private String userName;
    @Value("${tocco.api.password}")
    private String password;

    @Bean
    public RestTemplate toccoRestTemplate() {
        return createRestTemplate(userName, password);
    }

    @Bean
    public RestTemplate statusToccoRestTemplate() {
        return new RestTemplate(); // needs to be a bean so that we can mock it easily
    }
}
