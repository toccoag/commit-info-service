package ch.tocco.nice2.commit.jira;

import java.util.Map;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Issue {

    private String key;
    private String summary;
    private String status;
    private String projectType;

    public Issue() {
    }

    public Issue(String key, String summary) {
        this.key = key;
        this.summary = summary;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    @JsonProperty("fields")
    public void handleFields(Map<String, Object> fields) {
        this.summary = fields.get("summary").toString();
        Object status = fields.get("status");
        if (status instanceof Map) {
            this.status = ((Map) status).get("name").toString();
        }
        Object project = fields.get("project");
        if (project instanceof Map) {
            this.projectType = ((Map) project).get("projectTypeKey").toString();
        }
    }

    public String getSummary() {
        return summary;
    }

    public String getStatus() {
        return status;
    }

    public String getProjectType() {
        return projectType;
    }
}
