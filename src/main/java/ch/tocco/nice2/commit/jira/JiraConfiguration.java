package ch.tocco.nice2.commit.jira;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

import ch.tocco.nice2.commit.rest.AbstractRestConfiguration;

@Configuration
public class JiraConfiguration extends AbstractRestConfiguration {

    @Value("${jira.api.username}")
    private String userName;
    @Value("${jira.api.token}")
    private String accessToken;

    @Bean
    public RestTemplate jiraRestTemplate() {
        return createRestTemplate(userName, accessToken);
    }
}
