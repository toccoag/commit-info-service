package ch.tocco.nice2.commit.jira;

import java.util.List;
import java.util.Optional;
import java.util.regex.Pattern;

import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

@Component
public class JiraConnector {

    static final String SEARCH_JQL = "issuekey = %s or summary ~ %s";
    static final String SEARCH_JQL_NO_KEY = "summary ~ %s";

    @Autowired
    private RestTemplate jiraRestTemplate;

    @Value("${jira.api.url}")
    private String url;

    public Optional<Issue> fetchIssue(String key) {
        return Optional.ofNullable(jiraRestTemplate.getForObject(url + "/issue/" + key, Issue.class));
    }

    public IssueList searchIssues(String term) {
        String uri = UriComponentsBuilder.fromUriString(url + "/search")
            .queryParam("jql", mightBeIssueKey(term) ? String.format(SEARCH_JQL, term, term) : String.format(SEARCH_JQL_NO_KEY, term))
            .queryParam("maxResults", 10)
            .toUriString();
        return jiraRestTemplate.getForObject(uri, IssueList.class);
    }

    public List<Issue> searchAllIssues(String jql) {
        List<Issue> allIssues = Lists.newArrayList();

        int start = 0;
        int maxResults = 100;
        int total = Integer.MAX_VALUE;

        while (start < total) {
            String uri = UriComponentsBuilder.fromUriString(url + "/search")
                .queryParam("startAt", start)
                .queryParam("maxResults", maxResults)
                .queryParam("jql", jql)
                .toUriString();
            IssueList issueList = jiraRestTemplate.getForObject(uri, IssueList.class);

            if (issueList == null) {
                break;
            }

            allIssues.addAll(issueList.getIssues());

            maxResults = issueList.getMaxResults();
            total = issueList.getTotal();
            start += maxResults;
        }

        return allIssues;
    }

    private boolean mightBeIssueKey(String term) {
        return Pattern.matches("[A-Z0-9]+-[0-9]+", term);
    }
}
