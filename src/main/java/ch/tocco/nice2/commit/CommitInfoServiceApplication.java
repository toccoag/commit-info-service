package ch.tocco.nice2.commit;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.PropertySource;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
@PropertySource(value = "application.local.properties", ignoreResourceNotFound = true)
public class CommitInfoServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(CommitInfoServiceApplication.class, args);
    }
}
