package ch.tocco.nice2.commit.jira;

import java.util.Objects;

import org.hamcrest.BaseMatcher;
import org.hamcrest.Description;

public class IssueMatcher extends BaseMatcher<Issue> {

    private final String key;
    private final String summary;
    private final String status;
    private final String projectType;

    public static IssueMatcher isIssue(String key, String summary, String status, String projectType) {
        return new IssueMatcher(key, summary, status, projectType);
    }

    public static IssueMatcher isIssue(String key, String summary) {
        return new IssueMatcher(key, summary, null, null);
    }

    public static IssueMatcher isIssue(Issue issue) {
        return new IssueMatcher(issue.getKey(), issue.getSummary(), issue.getStatus(), issue.getProjectType());
    }

    private IssueMatcher(String key, String summary, String status, String projectType) {
        this.key = key;
        this.summary = summary;
        this.status = status;
        this.projectType = projectType;
    }

    @Override
    public boolean matches(Object item) {
        if (item instanceof Issue) {
            Issue issue = (Issue) item;
            return Objects.equals(key, issue.getKey()) && Objects.equals(summary, issue.getSummary()) && Objects.equals(status, issue.getStatus()) && Objects.equals(projectType, issue.getProjectType());
        }
        return false;
    }

    @Override
    public void describeTo(Description description) {
        description.appendText(String.format("Issue (%s, %s, %s, %s)", key, summary, status, projectType));
    }
}
