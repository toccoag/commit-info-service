package ch.tocco.nice2.commit.jira;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.util.StreamUtils;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriUtils;

import ch.tocco.nice2.commit.ExpectedExceptionMatcher;

import static ch.tocco.nice2.commit.jira.IssueMatcher.*;
import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.*;
import static org.hamcrest.collection.IsCollectionWithSize.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.*;
import static org.springframework.test.web.client.response.MockRestResponseCreators.*;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@ActiveProfiles("test")
public class JiraConnectorTests {

    @Autowired
    private RestTemplate jiraRestTemplate;

    @Autowired
    private JiraConnector jiraConnector;

    @Value("classpath:XYZ-1.json")
    private Resource issueResponse;

    @Value("classpath:Search.json")
    private Resource searchResponse;

    @Value("classpath:Search_all1.json")
    private Resource searchAll1;

    @Value("classpath:Search_all2.json")
    private Resource searchAll2;

    @Value("${jira.api.url}")
    private String url;

    private MockRestServiceServer mockServer;

    @BeforeEach
    public void setUp() {
        mockServer = MockRestServiceServer.createServer(jiraRestTemplate);
    }

    @Test
    public void fetchIssue() throws IOException {
        String key = "XYZ-1";

        String response = StreamUtils.copyToString(issueResponse.getInputStream(), Charset.forName("UTF-8"));

        mockServer.expect(requestTo(url + "issue/" + key))
            .andExpect(method(HttpMethod.GET))
            .andRespond(withSuccess(response, MediaType.APPLICATION_JSON));

        assertThat(jiraConnector.fetchIssue(key).get(), isIssue(key, "XYZ Task 1"));

        mockServer.verify();
    }

    @Test
    public void searchIssue() throws IOException {
        String term = "Search Term";

        String response = StreamUtils.copyToString(searchResponse.getInputStream(), Charset.forName("UTF-8"));

        mockServer.expect(requestTo(startsWith(url + "search")))
            .andExpect(method(HttpMethod.GET))
            .andExpect(queryParam("jql", UriUtils.encodeQueryParam(String.format(JiraConnector.SEARCH_JQL_NO_KEY, term), StandardCharsets.UTF_8.toString())))
            .andExpect(queryParam("maxResults", String.valueOf(10)))
            .andRespond(withSuccess(response, MediaType.APPLICATION_JSON));

        List<Issue> issue = jiraConnector.searchIssues(term).getIssues();

        assertThat(issue, hasSize(2));
        assertThat(issue, hasItem(isIssue("XYZ-1", "XYZ Task 1")));
        assertThat(issue, hasItem(isIssue("XYZ-2", "XYZ Task 2")));

        mockServer.verify();
    }

    @Test
    public void searchAllIssues() throws IOException {
        String jql = "in id (ABC-123, ABC-124, ABC-321, ABC-322)";

        String response1 = StreamUtils.copyToString(searchAll1.getInputStream(), Charset.forName("UTF-8"));
        String response2 = StreamUtils.copyToString(searchAll2.getInputStream(), Charset.forName("UTF-8"));

        mockServer.expect(requestTo(startsWith(url + "search")))
            .andExpect(method(HttpMethod.GET))
            .andExpect(queryParam("startAt", String.valueOf(0)))
            .andExpect(queryParam("maxResults", String.valueOf(100)))
            .andExpect(queryParam("jql", UriUtils.encodeQueryParam(jql, StandardCharsets.UTF_8.toString())))
            .andRespond(withSuccess(response1, MediaType.APPLICATION_JSON));

        mockServer.expect(requestTo(startsWith(url + "search")))
            .andExpect(method(HttpMethod.GET))
            .andExpect(queryParam("startAt", String.valueOf(2)))
            .andExpect(queryParam("maxResults", String.valueOf(2)))
            .andExpect(queryParam("jql", UriUtils.encodeQueryParam(jql, StandardCharsets.UTF_8.toString())))
            .andRespond(withSuccess(response2, MediaType.APPLICATION_JSON));

        List<Issue> issues = jiraConnector.searchAllIssues(jql);

        assertEquals(issues.size(), 4);
        assertThat(issues, hasItems(isIssue("ABC-123", "ABC Task 123", "in work", "software"),
            isIssue("ABC-124", "ABC Task 124", "ready for testing", "service_desk"),
            isIssue("ABC-321", "ABC Task 321", "closed", "business"),
            isIssue("ABC-322", "ABC Task 322", "ready for refinement", "software")));
        mockServer.verify();
    }

    @Test
    public void illegalRequest() {
        String key = "invalid_key";

        String response = "Invalid key!";

        mockServer.expect(requestTo(url + "issue/" + key))
            .andExpect(method(HttpMethod.GET))
            .andRespond(withBadRequest().body(response));

        assertThat(() -> jiraConnector.fetchIssue(key), new ExpectedExceptionMatcher(HttpClientErrorException.BadRequest.class, response));

        mockServer.verify();
    }
}
