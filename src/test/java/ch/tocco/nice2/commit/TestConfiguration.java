package ch.tocco.nice2.commit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;

import ch.tocco.nice2.commit.git.BackendGitExecutor;
import ch.tocco.nice2.commit.git.ClientGitExecutor;
import ch.tocco.nice2.commit.git.GitUpdater;

import static org.mockito.Mockito.*;

/**
 * This class may be used to override the autowiring of certain components or services for all integration tests
 */
@Profile("test")
@Configuration
public class TestConfiguration {
    @Bean
    @Primary
    public BackendGitExecutor backendGitExecutor() {
        return mock(BackendGitExecutor.class);
    }

    @Bean
    @Primary
    public ClientGitExecutor clientGitExecutor() {
        return mock(ClientGitExecutor.class);
    }

    @Bean
    @Primary
    public GitUpdater gitUpdater() {
        return mock(GitUpdater.class);
    }
}
