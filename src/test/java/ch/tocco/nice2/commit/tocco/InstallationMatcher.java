package ch.tocco.nice2.commit.tocco;

import java.util.Objects;

import org.hamcrest.BaseMatcher;
import org.hamcrest.Description;

public class InstallationMatcher extends BaseMatcher<Installation> {

    private final String instance;
    private final String name;
    private final String url;

    public static InstallationMatcher isInstallation(String instance, String name, String url) {
        return new InstallationMatcher(instance, name, url);
    }

    public static InstallationMatcher isInstallation(Installation installation) {
        return new InstallationMatcher(installation.getInstance(), installation.getLabel(), installation.getUrl());
    }

    private InstallationMatcher(String instance, String name, String url) {
        this.instance = instance;
        this.name = name;
        this.url = url;
    }

    @Override
    public boolean matches(Object item) {
        if (item instanceof Installation) {
            Installation installation = (Installation) item;
            return Objects.equals(instance, installation.getInstance()) &&
                Objects.equals(name, installation.getLabel()) &&
                Objects.equals(url, installation.getUrl());
        }

        return false;
    }

    @Override
    public void describeTo(Description description) {
        description.appendText(String.format("Installation (%s, %s, %s)", instance, name, url));
    }
}
