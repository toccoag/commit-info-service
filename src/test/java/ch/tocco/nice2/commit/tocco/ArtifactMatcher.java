package ch.tocco.nice2.commit.tocco;

import org.hamcrest.BaseMatcher;
import org.hamcrest.Description;

import java.util.Objects;

public class ArtifactMatcher extends BaseMatcher<Artifact> {

    private final String key;
    private final String nr;
    private final String label;
    private final String moduleLabel;
    private final String moduleTypeLabel;

    public static ArtifactMatcher isArtifact(String key, String nr, String label, String moduleLabel, String moduleTypeLabel) {
        return new ArtifactMatcher(key, nr, label, moduleLabel, moduleTypeLabel);
    }

    public static ArtifactMatcher isArtifact(Artifact artifact) {
        return new ArtifactMatcher(artifact.getKey(), artifact.getNr(), artifact.getLabel(), artifact.getModuleLabel(), artifact.getModuleTypeLabel());
    }

    public ArtifactMatcher(String key, String nr, String label, String moduleLabel, String moduleTypeLabel) {
        this.key = key;
        this.nr = nr;
        this.label = label;
        this.moduleLabel = moduleLabel;
        this.moduleTypeLabel = moduleTypeLabel;
    }

    @Override
    public boolean matches(Object item) {
        if (item instanceof Artifact) {
            Artifact artifact = (Artifact) item;
            return Objects.equals(key, artifact.getKey()) && Objects.equals(nr, artifact.getNr()) && Objects.equals(label, artifact.getLabel())
                    && Objects.equals(moduleLabel, artifact.getModuleLabel()) && Objects.equals(moduleTypeLabel, artifact.getModuleTypeLabel());
        }
        return false;
    }

    @Override
    public void describeTo(Description description) {
        description.appendText(String.format("Artifact (%s, %s, %s, %s, %s)", key, nr, label, moduleLabel, moduleTypeLabel));
    }
}
