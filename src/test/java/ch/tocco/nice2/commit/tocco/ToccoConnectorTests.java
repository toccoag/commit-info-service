package ch.tocco.nice2.commit.tocco;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.client.ExpectedCount;
import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.util.StreamUtils;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriUtils;

import static ch.tocco.nice2.commit.tocco.InstallationMatcher.*;
import static org.hamcrest.MatcherAssert.*;
import static org.hamcrest.Matchers.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.*;
import static org.springframework.test.web.client.response.MockRestResponseCreators.*;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@ActiveProfiles("test")
public class ToccoConnectorTests {

    @Autowired
    private RestTemplate toccoRestTemplate;

    @Autowired
    private RestTemplate statusToccoRestTemplate;

    @Autowired
    private ToccoConnector toccoConnector;

    @Value("classpath:installations.json")
    private Resource installationsResponse;

    @Value("classpath:status-tocco.html")
    private Resource statusToccoResponse;

    @Value("${tocco.api.url}")
    private String url;

    private MockRestServiceServer mockServer, statusToccoServer;

    @BeforeEach
    public void setUp() {
        mockServer = MockRestServiceServer.createServer(toccoRestTemplate);
        statusToccoServer = MockRestServiceServer.createServer(statusToccoRestTemplate);
    }

    @Test
    public void loadInstallations() throws IOException {
        String response = StreamUtils.copyToString(installationsResponse.getInputStream(), Charset.forName("UTF-8"));

        mockServer.expect(ExpectedCount.twice(), requestTo(startsWith(url + "entities/2.0/Installation")))
            .andExpect(method(HttpMethod.GET))
            .andExpect(queryParam("_paths", "instance,label,url"))
            .andExpect(queryParam("_where", UriUtils.encodeQueryParam("relInstallation_status.unique_id == string:\"active\" or relInstallation_status.unique_id == string:\"pilot\"", StandardCharsets.UTF_8.toString())))
            .andExpect(queryParam("_sort", "instance"))
            .andExpect(queryParam("_limit", String.valueOf(1000)))
            .andRespond(withSuccess(response, MediaType.APPLICATION_JSON));

        List<Installation> installations = toccoConnector.loadAllInstallations().getInstallations();
        assertThat(installations, hasSize(2));

        InstallationMatcher instA = isInstallation("instA", "Installation A", "https://instA.tocco.ch");
        InstallationMatcher instB = isInstallation("instB", "Installation B", "https://instB.tocco.ch");
        assertThat(installations, contains(instA, instB));

        assertThat(toccoConnector.loadInstallation("instA").get(), instA);
        assertThat(toccoConnector.loadInstallation("instB").get(), instB);

        //loaded From Cache
        assertSame(installations, toccoConnector.loadAllInstallations().getInstallations());

        //clearing the cache should trigger a new request
        toccoConnector.resetCache();
        toccoConnector.loadAllInstallations();

        mockServer.verify();
    }

    @Test
    public void installedRevisions() {
        String url = "http://localhost/";

        statusToccoServer.expect(requestTo(url + "status-tocco"))
            .andExpect(method(HttpMethod.GET))
            .andRespond(withSuccess(statusToccoResponse, MediaType.TEXT_HTML));

        Installation installation = new Installation("", "", url);

        String installedRevision = toccoConnector.getInstalledRevision(installation);
        assertThat(installedRevision, equalTo("0ddf7c0ab67bf381058d32653b446e2276bfb1f9"));
    }

    @Test
    public void getVersion() {
        String url = "http://localhost/";

        statusToccoServer.expect(requestTo(url + "status-tocco"))
                .andExpect(method(HttpMethod.GET))
                .andRespond(withSuccess(statusToccoResponse, MediaType.TEXT_HTML));

        Installation installation = new Installation("", "", url);

        VersionBean version = toccoConnector.getVersion(installation);
        assertThat(version.getVersion(), equalTo("3.3"));
        assertThat(version.getAdminVersion(), equalTo("1.0.55"));
    }
}
