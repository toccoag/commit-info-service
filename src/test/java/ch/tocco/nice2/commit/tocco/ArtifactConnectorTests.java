package ch.tocco.nice2.commit.tocco;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.util.StreamUtils;
import org.springframework.web.client.RestTemplate;

import ch.tocco.nice2.commit.rest.api.artifact.ArtifactsUpdateBean;

import static ch.tocco.nice2.commit.tocco.ArtifactMatcher.*;
import static org.hamcrest.MatcherAssert.*;
import static org.hamcrest.Matchers.*;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.*;
import static org.springframework.test.web.client.response.MockRestResponseCreators.*;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@ActiveProfiles("test")
public class ArtifactConnectorTests {

    @Autowired
    private RestTemplate toccoRestTemplate;

    @Autowired
    private ArtifactConnector artifactConnector;

    @Value("classpath:artefact_validator_page1.json")
    private Resource artifactValidatorPage1Response;

    @Value("classpath:artefact_validator_page2.json")
    private Resource artifactValidatorPage2Response;

    @Value("classpath:artefact_action.json")
    private Resource artifactActionResponse;

    @Value("classpath:artefact_else.json")
    private Resource artifactElseResponse;

    @Value("classpath:count0.json")
    private Resource count0Response;

    @Value("classpath:count1.json")
    private Resource count1Response;

    @Value("classpath:count2.json")
    private Resource count2Response;

    @Value("classpath:count4.json")
    private Resource count4Response;

    @Value("${tocco.api.url}")
    private String url;

    private MockRestServiceServer mockServer;

    @BeforeEach
    public void setUp() {
        mockServer = MockRestServiceServer.createServer(toccoRestTemplate);
    }

    @Test
    public void getArtifactsForIssue() throws IOException {
        String issueKey = "XYZ-1";

        for (ArtifactType type : ArtifactType.values()) {
            mockServer.expect(requestTo(startsWith(url + "entities/2.0/" + type.getModelName() + "/count")))
                .andExpect(method(HttpMethod.GET))
                .andExpect(queryParam("_where", "relJira_issue.issue_key%20%3D%3D%20string:%22" + issueKey + "%22"))
                .andRespond(withSuccess(getCountResponse(type), MediaType.APPLICATION_JSON));

            if (type == ArtifactType.ACTION || type == ArtifactType.VALIDATOR) {
                mockServer.expect(requestTo(startsWith(url + "entities/2.0/" + type.getModelName())))
                    .andExpect(method(HttpMethod.GET))
                    .andExpect(queryParam("_paths", "pk,nr,label,relModule.label,relModule.relModule_type.label"))
                    .andExpect(queryParam("_where", "relJira_issue.issue_key%20%3D%3D%20string:%22" + issueKey + "%22"))
                    .andExpect(queryParam("_sort", "nr"))
                    .andExpect(queryParam("_offset", String.valueOf(0)))
                    .andExpect(queryParam("_limit", String.valueOf(2)))
                    .andRespond(withSuccess(getResponse(type), MediaType.APPLICATION_JSON));
            }
        }

        Map<String, List<Artifact>> map = artifactConnector.getArtifactsForIssue(issueKey);

        ArtifactMatcher artifact1 = isArtifact("1", "11", "AHV Number Validator", "Person", "Zwischenmodul");
        ArtifactMatcher artifact2 = isArtifact("2", "22", "E-Mail Validator", "Adresse", "Core-Modul");
        assertThat(map.get("Artefact_validator"), contains(artifact1, artifact2));

        ArtifactMatcher artifact3 = isArtifact("3", "33", "API Key Action", "Benutzer", "Marketing-Modul");
        assertThat(map.get("Artefact_action"), contains(artifact3));

        List.of("Artefact_listener", "Artefact_report", "Artefact_flow", "Artefact", "Artefact_batchjob", "Artefact_directive").forEach(
            type -> assertThat(map.get(type), hasSize(0))
        );

        mockServer.verify();
    }

    @Test
    public void updateArtifactLinks() {
        String issueKey = "XYZ-1";

        mockServer.expect(requestTo(url + "tocco/artifact-update/" + issueKey))
            .andExpect(method(HttpMethod.PUT))
            .andRespond(withSuccess());

        artifactConnector.updateArtifactLinks(issueKey, new ArtifactsUpdateBean());

        mockServer.verify();
    }

    @Test
    public void getAllArtifacts() throws IOException {
        ArtifactType type = ArtifactType.VALIDATOR;

        mockServer.expect(requestTo(startsWith(url + "entities/2.0/" + type.getModelName() + "/count")))
            .andExpect(method(HttpMethod.GET))
            .andRespond(withSuccess(readResponseFromResource(count4Response), MediaType.APPLICATION_JSON));

        mockServer.expect(requestTo(startsWith(url + "entities/2.0/" + type.getModelName())))
            .andExpect(method(HttpMethod.GET))
            .andExpect(queryParam("_paths", "pk,nr,label,relModule.label,relModule.relModule_type.label"))
            .andExpect(queryParam("_sort", "nr"))
            .andExpect(queryParam("_offset", String.valueOf(0)))
            .andExpect(queryParam("_limit", String.valueOf(2)))
            .andRespond(withSuccess(readResponseFromResource(artifactValidatorPage1Response), MediaType.APPLICATION_JSON));

        mockServer.expect(requestTo(startsWith(url + "entities/2.0/" + type.getModelName())))
            .andExpect(method(HttpMethod.GET))
            .andExpect(queryParam("_paths", "pk,nr,label,relModule.label,relModule.relModule_type.label"))
            .andExpect(queryParam("_sort", "nr"))
            .andExpect(queryParam("_offset", String.valueOf(2)))
            .andExpect(queryParam("_limit", String.valueOf(2)))
            .andRespond(withSuccess(readResponseFromResource(artifactValidatorPage2Response), MediaType.APPLICATION_JSON));

        List<Artifact> artifacts = artifactConnector.getAllArtifacts(type.getModelName()).getArtifacts();
        assertThat(artifacts, hasSize(4));

        ArtifactMatcher artifact1 = isArtifact("1", "11", "AHV Number Validator", "Person", "Zwischenmodul");
        ArtifactMatcher artifact2 = isArtifact("2", "22", "E-Mail Validator", "Adresse", "Core-Modul");
        ArtifactMatcher artifact3 = isArtifact("3", "33", "Anderer Validator", "Person", "Zwischenmodul");
        ArtifactMatcher artifact4 = isArtifact("4", "44", "Noch ein Validator", "Adresse", "Core-Modul");
        assertThat(artifacts, contains(artifact1, artifact2, artifact3, artifact4));

        mockServer.verify();
    }

    private String getResponse(ArtifactType type) throws IOException {
        Resource resource;
        switch (type) {
            case VALIDATOR:
                resource = artifactValidatorPage1Response;
                break;
            case ACTION:
                resource = artifactActionResponse;
                break;
            default:
                resource = artifactElseResponse;
                break;
        }
        return readResponseFromResource(resource);
    }

    private String getCountResponse(ArtifactType type) throws IOException {
        Resource resource;
        switch (type) {
            case VALIDATOR:
                resource = count2Response;
                break;
            case ACTION:
                resource = count1Response;
                break;
            default:
                resource = count0Response;
                break;
        }
        return readResponseFromResource(resource);
    }

    private String readResponseFromResource(Resource resource) throws IOException {
        return StreamUtils.copyToString(resource.getInputStream(), StandardCharsets.UTF_8);
    }
}
