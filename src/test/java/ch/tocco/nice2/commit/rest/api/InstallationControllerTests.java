package ch.tocco.nice2.commit.rest.api;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.bean.override.mockito.MockitoBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.web.client.RestTemplate;

import ch.tocco.nice2.commit.git.ClientGitConnector;
import ch.tocco.nice2.commit.git.Commit;
import ch.tocco.nice2.commit.git.BackendGitConnector;
import ch.tocco.nice2.commit.git.CommitInstalled;
import ch.tocco.nice2.commit.jira.Issue;
import ch.tocco.nice2.commit.jira.JiraConnector;
import ch.tocco.nice2.commit.rest.AbstractRestConfiguration;
import ch.tocco.nice2.commit.rest.api.installation.CommitInstalledBean;
import ch.tocco.nice2.commit.rest.api.issue.CommitsBean;
import ch.tocco.nice2.commit.tocco.Installation;
import ch.tocco.nice2.commit.tocco.InstallationList;
import ch.tocco.nice2.commit.tocco.ToccoConnector;
import ch.tocco.nice2.commit.tocco.VersionBean;

import static ch.tocco.nice2.commit.jira.IssueMatcher.*;
import static ch.tocco.nice2.commit.tocco.InstallationMatcher.*;
import static org.hamcrest.MatcherAssert.*;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.*;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
public class InstallationControllerTests {

    @LocalServerPort
    private int port;

    @Autowired
    private RestTemplate restClient;

    @MockitoBean
    private ToccoConnector toccoConnector;

    @MockitoBean
    private BackendGitConnector backendGitConnector;

    @MockitoBean
    private ClientGitConnector clientGitConnector;


    @MockitoBean
    private JiraConnector jiraConnector;

    @Test
    public void getInstallations() {
        InstallationList installations = new InstallationList();
        Installation installation1 = new Installation("test", "Test Installation", "http://localhost/test");
        Installation installation2 = new Installation("test2", "Test Installation 2", "http://localhost/test2");
        installations.setInstallations(List.of(installation1, installation2));

        when(toccoConnector.loadAllInstallations()).thenReturn(installations);

        List<Installation> list = restClient.getForObject("http://localhost:" + port + "/installation", InstallationList.class).getInstallations();
        assertThat(list, hasSize(2));
        assertThat(list, contains(isInstallation(installation1), isInstallation(installation2)));
    }

    @Test
    public void getInstallation() {
        Installation installation = new Installation("test", "Test Installation", "http://localhost/test");

        when(toccoConnector.loadInstallation(installation.getInstance())).thenReturn(Optional.of(installation));

        Installation result = restClient.getForObject("http://localhost:" + port + "/installation/" + installation.getInstance(), Installation.class);
        assertThat(result, isInstallation(installation));
    }

    @Test
    public void isInstalled() {
        Installation installation = new Installation("test", "Test Installation", "http://localhost/test");
        Commit commit = new Commit();
        commit.setCommitId("abcdef0123456789");
        commit.setCommitMessage("message");

        String installationHash = "01234abcd";

        when(toccoConnector.loadInstallation(installation.getInstance())).thenReturn(Optional.of(installation));
        when(backendGitConnector.retrieveCommit(commit.getCommitId())).thenReturn(Optional.of(commit));
        when(toccoConnector.getInstalledRevision(installation)).thenReturn(installationHash);
        when(backendGitConnector.isAncestor(commit.getCommitId(), installationHash)).thenReturn(true);

        CommitInstalledBean result = restClient.getForObject(
            String.format("http://localhost:%d/installation/%s/commit/%s", port, installation.getInstance(), commit.getCommitId()),
            CommitInstalledBean.class
        );
        assertNotNull(result);
        assertTrue(result.isInstalled());
        assertEquals(commit.getCommitMessage(), result.getCommitMessage());
        assertEquals(commit.getCommitId(), result.getCommitHash());
        assertEquals(installation.getLabel(), result.getInstallation());
    }

    @Test
    public void getClientCommits() {
        Installation installation = new Installation("test", "Test Installation", "http://localhost/test");
        String issueKey = "TOCDEV-1";
        Issue issue = new Issue(issueKey, "First issue");
        VersionBean version = new VersionBean();
        version.setAdminVersion("1.0.55");
        CommitInstalled commit = new CommitInstalled();
        commit.setCommitId("abcdef0123456789");
        commit.setCommitMessage("message");
        commit.setInstalled(true);

        when(jiraConnector.fetchIssue(issueKey)).thenReturn(Optional.of(issue));
        when(toccoConnector.loadInstallation(installation.getInstance())).thenReturn(Optional.of(installation));
        when(toccoConnector.getVersion(installation)).thenReturn(version);
        when(clientGitConnector.searchCommitsForInstallation(eq(version), ArgumentMatchers.startsWith(issueKey))).thenReturn(List.of(commit));

        CommitsBean result = restClient.getForObject(
                String.format("http://localhost:%d/installation/%s/client/issue/%s", port, installation.getInstance(), issueKey),
                CommitsBean.class
        );
        assertNotNull(result);
        assertThat(result.getCommits(), hasSize(1));
    }

    @TestConfiguration
    static class RestTestConfiguration extends AbstractRestConfiguration {

        @Value("${rest.api.username}")
        private String username;

        @Value("${rest.api.password}")
        private String password;

        @Bean("restClient")
        public RestTemplate restClient() {
            return createRestTemplate(username, password);
        }
    }
}
