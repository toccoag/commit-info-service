package ch.tocco.nice2.commit.rest.api;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.bean.override.mockito.MockitoBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.web.client.RestTemplate;

import ch.tocco.nice2.commit.rest.AbstractRestConfiguration;
import ch.tocco.nice2.commit.rest.api.artifact.ArtifactsBean;
import ch.tocco.nice2.commit.rest.api.artifact.ArtifactsUpdateBean;
import ch.tocco.nice2.commit.tocco.Artifact;
import ch.tocco.nice2.commit.tocco.ArtifactConnector;
import ch.tocco.nice2.commit.tocco.ArtifactList;
import ch.tocco.nice2.commit.tocco.ArtifactType;

import static ch.tocco.nice2.commit.tocco.ArtifactMatcher.*;
import static org.hamcrest.MatcherAssert.*;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
public class ArtifactControllerTests {

    @LocalServerPort
    private int port;

    @Autowired
    private RestTemplate restClient;

    @MockitoBean
    private ArtifactConnector artifactConnector;

    @Test
    public void getAllArtifacts() {
        String modelName = ArtifactType.VALIDATOR.getModelName();

        ArtifactList artifacts = new ArtifactList();
        Artifact artifact1 = new Artifact("1", "11", "AHV Number Validator", "Person", "Zwischenmodul");
        Artifact artifact2 = new Artifact("2", "22", "E-Mail Validator", "Adresse", "Core-Modul");
        artifacts.setArtifacts(List.of(artifact1, artifact2));

        when(artifactConnector.getAllArtifacts(modelName)).thenReturn(artifacts);

        List<Artifact> list = restClient.getForObject(String.format("http://localhost:%s/artifact/%s", port, modelName), ArtifactList.class).getArtifacts();
        assertThat(list, hasSize(2));
        assertThat(list, contains(isArtifact(artifact1), isArtifact(artifact2)));
    }

    @Test
    public void getArtifacts() {
        String issueKey = "XYZ-1";
        String validatorArtifact = ArtifactType.VALIDATOR.getModelName();
        String validatorAction = ArtifactType.ACTION.getModelName();

        Map<String, List<Artifact>> artifacts = new HashMap<>();
        Artifact artifact1 = new Artifact("1", "11", "AHV Nummer Validator", "Person", "Zwischenmodul");
        Artifact artifact2 = new Artifact("2", "22", "E-Mail Validator", "Adresse", "Core-Modul");
        artifacts.put(validatorArtifact, List.of(artifact1, artifact2));
        Artifact artifact3 = new Artifact("3", "33", "API Key Action", "Benutzer", "Marketing-Modul");
        artifacts.put(validatorAction, List.of(artifact3));

        when(artifactConnector.getArtifactsForIssue(issueKey)).thenReturn(artifacts);

        Map<String, List<Artifact>> map = restClient.getForObject(String.format("http://localhost:%s/artifact/issue/%s", port, issueKey), ArtifactsBean.class).getArtifacts();
        assertEquals(map.keySet().size(), 2);
        assertThat(map.get(validatorArtifact), contains(isArtifact(artifact1), isArtifact(artifact2)));
        assertThat(map.get(validatorAction), contains(isArtifact(artifact3)));
    }

    @Test
    public void putArtifacts() {
        String issueKey = "XYZ-1";
        String validatorArtifact = ArtifactType.VALIDATOR.getModelName();
        String validatorAction = ArtifactType.ACTION.getModelName();

        ArtifactsUpdateBean bean = new ArtifactsUpdateBean();
        Map<String, List<String>> artifacts = new HashMap<>();
        artifacts.put(validatorArtifact, List.of("1", "2"));
        artifacts.put(validatorAction, List.of("3"));
        bean.setArtifacts(artifacts);

        HttpEntity<ArtifactsUpdateBean> requestUpdate = new HttpEntity<>(bean);
        restClient.exchange(String.format("http://localhost:%s/artifact/issue/%s", port, issueKey), HttpMethod.PUT, requestUpdate, Void.class);
        verify(artifactConnector, times(1)).updateArtifactLinks(issueKey, bean);
    }

    @TestConfiguration
    static class RestTestConfiguration extends AbstractRestConfiguration {

        @Value("${rest.api.username}")
        private String username;

        @Value("${rest.api.password}")
        private String password;

        @Bean("restClient")
        public RestTemplate restClient() {
            return createRestTemplate(username, password);
        }
    }
}
