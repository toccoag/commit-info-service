package ch.tocco.nice2.commit.rest.api;

import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.context.annotation.Bean;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.bean.override.mockito.MockitoBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.web.client.RestTemplate;

import ch.tocco.nice2.commit.git.ClientGitConnector;
import ch.tocco.nice2.commit.git.Commit;
import ch.tocco.nice2.commit.git.BackendGitConnector;
import ch.tocco.nice2.commit.jira.Issue;
import ch.tocco.nice2.commit.jira.IssueList;
import ch.tocco.nice2.commit.jira.JiraConnector;
import ch.tocco.nice2.commit.rest.AbstractRestConfiguration;
import ch.tocco.nice2.commit.rest.api.issue.CommitsBean;
import ch.tocco.nice2.commit.rest.api.issue.SearchBean;

import static ch.tocco.nice2.commit.jira.IssueMatcher.*;
import static org.hamcrest.MatcherAssert.*;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
public class IssueControllerTests {

    @LocalServerPort
    private int port;

    @Autowired
    private RestTemplate restClient;

    @MockitoBean
    private JiraConnector jiraConnector;

    @MockitoBean
    private BackendGitConnector backendGitConnector;

    @MockitoBean
    private ClientGitConnector clientGitConnector;

    @Test
    public void searchIssues() {
        IssueList issues = new IssueList();
        Issue issue1 = new Issue("ABC-123", "Issue 1");
        Issue issue2 = new Issue("ABC-234", "Issue 1");
        issues.setIssues(List.of(issue1, issue2));

        String searchTerm = "search";

        when(jiraConnector.searchIssues(searchTerm)).thenReturn(issues);

        SearchBean searchBean = new SearchBean();
        searchBean.setSearchTerm(searchTerm);

        ResponseEntity<IssueList> response = restClient.postForEntity("http://localhost:" + port + "/issue/search", searchBean, IssueList.class);
        List<Issue> list = response.getBody().getIssues();
        assertThat(list, hasSize(2));
        assertThat(list, contains(isIssue(issue1), isIssue(issue2)));
    }

    @Test
    public void getIssue() {
        String key = "ABC-123";
        Issue issue = new Issue(key, "Issue 1");

        when(jiraConnector.fetchIssue(key)).thenReturn(Optional.of(issue));

        Issue result = restClient.getForObject("http://localhost:" + port + "/issue/" + key, Issue.class);
        assertThat(result, isIssue(issue));
    }

    @Test
    public void commits() {
        Commit commit = new Commit();
        commit.setCommitId("abcdef0123456789");
        commit.setCommitMessage("message");

        Commit commit2 = new Commit();
        commit2.setCommitId("abcd1234abcd1234");
        commit2.setCommitMessage("message2");

        Commit commit3 = new Commit();
        commit3.setCommitId("xyz0987654321");
        commit3.setCommitMessage("message3");

        String issueKey = "ABC-123";
        when(jiraConnector.fetchIssue(issueKey)).thenReturn(Optional.of(new Issue(issueKey, "")));
        when(backendGitConnector.searchCommits(issueKey + "\\b")).thenReturn(List.of(commit, commit2));
        when(clientGitConnector.searchCommits(issueKey + "\\b")).thenReturn(List.of(commit3));

        CommitsBean result = restClient.getForObject(
            String.format("http://localhost:%d/issue/%s/commit", port, issueKey),
            CommitsBean.class
        );
        assertNotNull(result);
        assertThat(result.getCommits(), hasSize(3));
    }

    @TestConfiguration
    static class RestTestConfiguration extends AbstractRestConfiguration {

        @Value("${rest.api.username}")
        private String username;

        @Value("${rest.api.password}")
        private String password;

        @Bean("restClient")
        public RestTemplate restClient() {
            return createRestTemplate(username, password);
        }
    }
}
