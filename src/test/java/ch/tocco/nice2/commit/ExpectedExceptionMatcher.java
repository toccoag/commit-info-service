package ch.tocco.nice2.commit;

import org.hamcrest.BaseMatcher;
import org.hamcrest.Description;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.*;
import static org.junit.jupiter.api.Assertions.*;

public class ExpectedExceptionMatcher extends BaseMatcher<Runnable> {

    private final Class<? extends Throwable> expectedException;
    private final String message;

    public ExpectedExceptionMatcher(Class<? extends Throwable> expectedException, String message) {
        this.expectedException = expectedException;
        this.message = message;
    }

    @Override
    public boolean matches(Object item) {
        if (item instanceof Runnable) {
            Runnable runnable = (Runnable) item;
            try {
                runnable.run();
            } catch (Throwable throwable) {
                assertEquals(throwable.getClass(), expectedException);
                assertThat(throwable.getMessage(), containsString(message));
                return true;
            }
        }

        return false;
    }

    @Override
    public void describeTo(Description description) {
        description.appendText(String.format("Expected Exception: %s", expectedException.getName()));
    }
}
