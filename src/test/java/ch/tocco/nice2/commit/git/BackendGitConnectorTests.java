package ch.tocco.nice2.commit.git;

import java.io.IOException;
import java.io.InputStream;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.stubbing.Answer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.Resource;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@ActiveProfiles("test")
public class BackendGitConnectorTests {
    @Autowired
    private BackendGitExecutor backendGitExecutor;

    @Autowired
    private BackendGitConnector backendGitConnector;

    @Value("classpath:commit.txt")
    private Resource commit;

    @Value("classpath:search_result.txt")
    private Resource searchResult;

    @Test
    public void testIsAncestor() throws IOException, InterruptedException {
        runIsAncestor(0, true);
    }
    @Test
    public void testIsNotAncestor() throws IOException, InterruptedException {
        runIsAncestor(1, false);
    }

    private void runIsAncestor(int exitCode, boolean expectedOutput) throws InterruptedException, IOException {
        Process process = mock(Process.class);
        when(process.waitFor()).thenReturn(exitCode);
        when(backendGitExecutor.executeGitCommand(List.of("git", "merge-base", "--is-ancestor", "%rev1%", "%rev2%"), Map.of("%rev1%", "0ddf7c0ab67bf381058d32653b446e2276bfb1f9", "%rev2%", "ea3b3415322fafbca23bfe2c73631201d221480a"))).thenReturn(process);

        boolean output = backendGitConnector.isAncestor("0ddf7c0ab67bf381058d32653b446e2276bfb1f9", "ea3b3415322fafbca23bfe2c73631201d221480a");
        assertEquals(expectedOutput, output);
    }

    @Test
    public void testRetrieveCommit() throws InterruptedException, IOException {
        when(backendGitExecutor.executeAndProcessError(eq(List.of("git", "log", "--abbrev-commit", "--full-index", "--date=iso-strict",  "-n", "1", "%commitId%")), any(), eq(Map.of("%commitId%", "9d98b7a5ce67984594725a29d2099d81c3023a4f"))))
            .then((Answer<?>) invocation ->  {
                Function<InputStream, ?> argument = invocation.getArgument(1);
                return argument.apply(commit.getInputStream());
            });

        Commit commit = backendGitConnector.retrieveCommit("9d98b7a5ce67984594725a29d2099d81c3023a4f")
            .orElseThrow(() -> new GitException("Commit not found"));
        assertEquals("9d98b7a5ce67984594725a29d2099d81c3023a4f", commit.getCommitId());
        assertEquals("Hans Meier <hmeier@test.ch>", commit.getAuthor());
        assertEquals(LocalDateTime.of(2019, 06, 05, 17, 20, 12), commit.getCommitTimestamp());
        assertEquals("support detail documents as _path param in new EntitiesResource\n" +
            "\n" +
            "- add BinaryDataConverter which converts binary data to binary bean\n" +
            "- make sure ResourceNodeImpl accepts the field name as file name as well,\n" +
            "  as the generated file name would require generating default displays\n" +
            "  which is slow\n" +
            "\n" +
            "Change-Id: I7c1c8291bf8308d4a6f44f18116e326c808d6e71\n" +
            "Refs: 1234567, TOCDEV-229\n" +
            "Reviewed-on: https://git.tocco.ch/32070\n" +
            "Tested-by: Teamcity CI <teamcity@tocco.ch>\n" +
            "Reviewed-by: Max Müller <mmueller@test.ch>", commit.getCommitMessage());

        assertEquals(1, commit.getJiraIssueKeys().size());
        assertEquals("TOCDEV-229", commit.getJiraIssueKeys().get(0));
    }

    @Test
    public void testSearchCommits() throws IOException, InterruptedException {
        when(backendGitExecutor.executeAndProcessError(eq(List.of("git", "log", "--all", "--abbrev-commit", "--full-index", "--no-merges", "--date=iso-strict", "--grep=%regexp%")), any(), eq(Map.of("%regexp%", "TOCDEV-229")))).then((Answer<?>) invocation ->  {
            Function<InputStream, ?> argument = invocation.getArgument(1);
            return argument.apply(searchResult.getInputStream());
        });

        List<Commit> commits = backendGitConnector.searchCommits("TOCDEV-229");
        assertEquals(3, commits.size());

        List<String> expectedCommits = List.of("9d98b7a5ce67984594725a29d2099d81c3023a4f", "e9d13408b2886b613c8b2480ce947a7b5a6567b1", "9bbe52da5cc2324cb68b07b4e3553b412ae267e6");

        List<String> actualCommits = commits.stream()
            .map(Commit::getCommitId)
            .collect(Collectors.toList());

        assertTrue(expectedCommits.containsAll(actualCommits));
    }
}
