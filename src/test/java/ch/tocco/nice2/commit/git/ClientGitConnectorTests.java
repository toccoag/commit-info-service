package ch.tocco.nice2.commit.git;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.stubbing.Answer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.Resource;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import ch.tocco.nice2.commit.tocco.VersionBean;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.*;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@ActiveProfiles("test")
public class ClientGitConnectorTests {
    @Autowired
    private ClientGitExecutor clientGitExecutor;

    @Autowired
    private ClientGitConnector clientGitConnector;

    @Value("classpath:search_result.txt")
    private Resource searchResult;

    @Test
    public void testSearchCommitsForInstallation() throws IOException, InterruptedException {
        String currentTagCommitId = "9bbe52da5cc2324cb68b07b4e3553b412ae267e6";

        when(clientGitExecutor.executeAndProcessError(eq(List.of("git", "log", "%branch%", "--abbrev-commit", "--full-index", "--no-merges", "--date=iso-strict", "--grep=%regexp%")), any(), eq(Map.of("%branch%", "master", "%regexp%", "TOCDEV-229\\b")))).then((Answer<?>) invocation -> {
            Function<InputStream, ?> argument = invocation.getArgument(1);
            return argument.apply(searchResult.getInputStream());
        });

        Process process = mock(Process.class);
        when(process.waitFor()).thenReturn(1);
        when(clientGitExecutor.executeGitCommand(List.of("git", "rev-parse", "--verify", "%branch%"), Map.of("%branch%", "nice-releases/33"))).thenReturn(process);

        when(clientGitExecutor.executeAndProcessError(eq(List.of("git", "log", "%branch%", "--grep=%package-version%", "-n", "1", "--pretty=format:%h")), any(), eq(Map.of("%package-version%", "tocco-admin@1.0.55", "%branch%", "master")))).then((Answer<?>) invocation -> {
            Function<InputStream, ?> argument = invocation.getArgument(1);
            return argument.apply(new ByteArrayInputStream(currentTagCommitId.getBytes()));
        });

        mockIsAncestor(false, "9bbe52da5cc2324cb68b07b4e3553b412ae267e6", currentTagCommitId);
        mockIsAncestor(true, "e9d13408b2886b613c8b2480ce947a7b5a6567b1", currentTagCommitId);
        mockIsAncestor(true, "9d98b7a5ce67984594725a29d2099d81c3023a4f", currentTagCommitId);

        VersionBean version = new VersionBean();
        version.setVersion("3.3");
        version.setAdminVersion("1.0.55");
        clientGitConnector.searchCommitsForInstallation(version, "TOCDEV-229\\b");
    }

    @Test
    public void testSearchCommits() throws IOException, InterruptedException {
        when(clientGitExecutor.executeAndProcessError(eq(List.of("git", "log", "%branch%", "--abbrev-commit", "--full-index", "--no-merges", "--date=iso-strict", "--grep=%regexp%")), any(), eq(Map.of("%branch%", "master", "%regexp%", "TOCDEV-229\\b")))).then((Answer<?>) invocation -> {
            Function<InputStream, ?> argument = invocation.getArgument(1);
            return argument.apply(searchResult.getInputStream());
        });

        List<Commit> commits = clientGitConnector.searchCommits("TOCDEV-229\\b");
        assertEquals(3, commits.size());

        List<String> expectedCommits = List.of("9d98b7a5ce67984594725a29d2099d81c3023a4f", "e9d13408b2886b613c8b2480ce947a7b5a6567b1", "9bbe52da5cc2324cb68b07b4e3553b412ae267e6");

        List<String> actualCommits = commits.stream()
                .map(Commit::getCommitId)
                .toList();

        assertTrue(expectedCommits.containsAll(actualCommits));
    }

    private void mockIsAncestor(boolean isAncestor, String rev1, String rev2) throws InterruptedException, IOException {
        Process process = mock(Process.class);
        when(process.waitFor()).thenReturn(isAncestor ? 0 : 1);
        when(clientGitExecutor.executeGitCommand(List.of("git", "merge-base", "--is-ancestor", "%rev1%", "%rev2%"), Map.of("%rev1%", rev1, "%rev2%", rev2))).thenReturn(process);
    }
}
