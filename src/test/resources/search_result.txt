commit 9d98b7a5ce67984594725a29d2099d81c3023a4f
Author: Hans Meier <hmeier@test.ch>
Date:   2019-06-05T17:20:12+02:00

    support detail documents as _path param in new EntitiesResource
    
    - add BinaryDataConverter which converts binary data to binary bean
    - make sure ResourceNodeImpl accepts the field name as file name as well,
      as the generated file name would require generating default displays
      which is slow
    
    Change-Id: I7c1c8291bf8308d4a6f44f18116e326c808d6e71
    Refs: TOCDEV-229
    Reviewed-on: https://git.tocco.ch/32070
    Tested-by: Teamcity CI <teamcity@tocco.ch>
    Reviewed-by: Max Müller <mmueller@test.ch>

dms/api/src/main/java/ch/tocco/nice2/dms/tree/ContentTreeNode.java
dms/impl/src/main/java/ch/tocco/nice2/dms/impl/tree/uri/AdminUriResolveStrategy.java
dms/spi/src/main/java/ch/tocco/nice2/dms/spi/tree/AbstractContentTreeNode.java
dms/spi/src/main/java/ch/tocco/nice2/dms/spi/tree/ResourceNodeImpl.java
optional/cms/impl/src/test/java/ch/tocco/nice2/optional/cms/impl/tree/uri/AdminUriResolveStrategyTest.java
optional/cms/impl/src/test/java/ch/tocco/nice2/optional/cms/impl/util/PageNodeMockBuilder.java
rest/entity/impl/pom.xml
rest/entity/impl/src/main/java/ch/tocco/nice2/rest/entity/impl/beans/factories/v2/BinaryDataConverter.java
rest/entity/module/descriptor/hivemodule.xml

commit e9d13408b2886b613c8b2480ce947a7b5a6567b1
Author: Hans Meier <hmeier@test.ch>
Date:   2019-06-04T16:44:56+02:00

    improve querying for binaries using the CriteriaQueryBuilder
    
    - refactor internals to support additional handlers for special paths and
      introduce a selection registry to avoid duplicate selections and cleaner
      handling of path indices
    - add special handler for binary fields to support loading binary metadata
      efficiently (instead of through the user type, which fires a query for
      every single binary)
    
    Change-Id: I777b3ff89cb75f10ed07ab78982416a481bbf714
    Refs: TOCDEV-229
    Reviewed-on: https://git.tocco.ch/32061
    Tested-by: Teamcity CI <teamcity@tocco.ch>
    Reviewed-by: Max Müller <mmueller@test.ch>

persist/core/hibernate-api/src/main/java/ch/tocco/nice2/persist/hibernate/query/CriteriaQueryBuilder.java
persist/core/hibernate-api/src/main/java/ch/tocco/nice2/persist/hibernate/query/selection/AbstractPathBasedQuerySelection.java
persist/core/hibernate-api/src/main/java/ch/tocco/nice2/persist/hibernate/query/selection/AbstractQuerySelection.java
persist/core/hibernate-api/src/main/java/ch/tocco/nice2/persist/hibernate/query/selection/AbstractToManySelectionPathHandler.java
persist/core/hibernate-api/src/main/java/ch/tocco/nice2/persist/hibernate/query/selection/CustomSelection.java
persist/core/hibernate-api/src/main/java/ch/tocco/nice2/persist/hibernate/query/selection/Path.java
persist/core/hibernate-api/src/main/java/ch/tocco/nice2/persist/hibernate/query/selection/PluralPathQuerySelection.java
persist/core/hibernate-api/src/main/java/ch/tocco/nice2/persist/hibernate/query/selection/QuerySelection.java
persist/core/hibernate-api/src/main/java/ch/tocco/nice2/persist/hibernate/query/selection/SelectionPathHandler.java
persist/core/hibernate-api/src/main/java/ch/tocco/nice2/persist/hibernate/query/selection/SelectionRegistry.java
persist/core/hibernate-api/src/main/java/ch/tocco/nice2/persist/hibernate/query/selection/SingularPathQuerySelection.java
persist/core/hibernate-api/src/main/java/ch/tocco/nice2/persist/hibernate/query/selection/ToManyBinarySelectionPathHandler.java
persist/core/hibernate-api/src/main/java/ch/tocco/nice2/persist/hibernate/query/selection/ToManySelectionPathHandler.java
persist/core/hibernate-api/src/main/java/ch/tocco/nice2/persist/hibernate/query/selection/ToOneBinarySelectionPathHandler.java
persist/core/hibernate-api/src/main/java/ch/tocco/nice2/persist/hibernate/query/selection/ToOneSelectionPathHandler.java
persist/core/hibernate-impl/src/main/java/ch/tocco/nice2/persist/hibernate/pojo/PersistenceServiceImpl.java
persist/core/hibernate-testlib/src/main/java/ch/tocco/nice2/persist/hibernate/test/HibernateModule.java
persist/core/hibernate-testlib/src/test/java/ch/tocco/nice2/persist/hibernate/binary/BinaryDataAccessorTest.java
persist/core/hibernate-testlib/src/test/java/ch/tocco/nice2/persist/hibernate/query/QueryBuilderInterceptorSelectionResultTest.java

commit 9bbe52da5cc2324cb68b07b4e3553b412ae267e6
Author: Hans Meier <hmeier@test.ch>
Date:   2019-05-27T15:56:39+02:00

    preparations for retrieving binary information for the REST api efficiently
    
    - move building of admin urls to external service so that they can be built
      efficiently without having to create AbstractContentTreeNode instances
    - add BinaryDataAccessor service to be able to fetch all binary information
      for a detail document in one query
    
    Change-Id: I49cca3423670137f6369e123966221f23a3ed035
    Refs: TOCDEV-229
    Reviewed-on: https://git.tocco.ch/32009
    Reviewed-by: Max Müller <mmueller@test.ch>
    Tested-by: Teamcity CI <teamcity@tocco.ch>

dms/impl/src/test/java/ch/tocco/nice2/dms/impl/dbrefactoring/RepairReferenceCountServiceTest.java
dms/impl/src/test/java/ch/tocco/nice2/dms/impl/inject/ContentTreeServiceModule.java
dms/impl/src/test/java/ch/tocco/nice2/dms/impl/inject/NodeServicePointServices.java
dms/impl/src/test/java/ch/tocco/nice2/dms/impl/maintenance/DeleteUnreferencedBinariesBatchJobTest.java
dms/impl/src/test/java/ch/tocco/nice2/dms/impl/maintenance/RepairReferenceCountBatchJobTest.java
dms/module/descriptor/hivemodule.xml
dms/spi/src/main/java/ch/tocco/nice2/dms/spi/tree/AbstractContentTreeNode.java
dms/spi/src/main/java/ch/tocco/nice2/dms/spi/tree/BinaryContentImpl.java
dms/spi/src/main/java/ch/tocco/nice2/dms/spi/tree/DmsUrlBuilder.java
dms/spi/src/main/java/ch/tocco/nice2/dms/spi/tree/DmsUrlBuilderImpl.java
optional/s3storage/impl/src/test/java/ch/tocco/nice2/optional/s3/storage/NiceBinaryTest.java
persist/core/hibernate-api/src/main/java/ch/tocco/nice2/persist/hibernate/binary/BinaryData.java
persist/core/hibernate-api/src/main/java/ch/tocco/nice2/persist/hibernate/binary/BinaryDataAccessor.java
persist/core/hibernate-impl/src/main/java/ch/tocco/nice2/persist/hibernate/binary/BinaryDataAccessorImpl.java
persist/core/hibernate-testlib/src/main/java/ch/tocco/nice2/persist/hibernate/binary/AbstractNiceBinaryTest.java
persist/core/hibernate-testlib/src/test/java/ch/tocco/nice2/persist/hibernate/binary/BinaryDataAccessorTest.java
persist/core/module/descriptor/hivemodule.xml
